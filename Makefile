MAKEFLAGS += --no-builtin-rules

include config.mak

TARGET = libmg2d.so

TESTPROGS = $(addsuffix _test,  \
    relax                       \
    relax_mpi                   \
    mg2d                        \
    mg2d_mpi                    \
)

VPATH = $(SOURCE_DIR)

CFLAGS += -std=c11 -fPIC
CFLAGS += -g -O3
CFLAGS += -D_XOPEN_SOURCE=700                      # POSIX
CFLAGS += -D_DEFAULT_SOURCE=1                      # for random_r
CFLAGS += -I. -I$(SOURCE_DIR)
CFLAGS += $(CFLAGS_DEP) $(CFLAGS_EXTRA)

NASMFLAGS += -Dprivate_prefix=mg2di

LDFLAGS_COMMON  = $(LDFLAGS_DEP) $(LDFLAGS_EXTRA)
LDFLAGS_COMMON += -lm -llapacke -lblas -lthreadpool -lndarray
LDFLAGS_TARGET  = -shared -Wl,--version-script=$(SOURCE_DIR)/libmg2d.v

OBJS =                          \
       bicgstab.c.o             \
       boundary.c.o             \
       components.c.o           \
       cpu.c.o                  \
       ell_grid_solve.c.o       \
       log.c.o                  \
       mg2d.c.o                 \
       residual_calc.c.o        \
       timer.c.o                \
       transfer.c.o

OBJS-$(HAVE_NASM) =             \
       cpuid.asm.o              \
       ndarray.asm.o            \
       residual_calc.asm.o      \
       transfer_interp.asm.o    \

OBJS += $(OBJS-yes)

all: $(TARGET)

testprogs: $(TESTPROGS)

$(TARGET) $(TESTPROGS): $(OBJS)
	$(CC) -o $@ $(OBJS) $(TESTOBJ) $(LDFLAGS_COMMON) $(LDFLAGS)

$(TARGET): LDFLAGS = $(LDFLAGS_TARGET)

$(TESTPROGS): %: %.c.o
$(TESTPROGS): $(TARGET)
$(foreach testprog, $(TESTPROGS), $(eval $(testprog): TESTOBJ = $(testprog).c.o))

%.c.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

%.asm.o: %.asm
	nasm $(NASMFLAGS) -i $(SOURCE_DIR)/ -f elf64 -MD $(@:.o=.d) -MT $@ -o $@ $<

clean:
	-rm -f *.o *.d *.pyc $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: all testprogs clean
