/*
 * Multi-component utility code
 * Copyright 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_COMPONENTS_H
#define MG2D_COMPONENTS_H

#include <stdint.h>
#include <stdlib.h>

typedef struct Rect {
    ptrdiff_t start[2];
    size_t    size[2];
} Rect;

typedef struct DomainComponent {
    /* points that belong to this component */
    Rect interior;
    /* interior + ghost points on the physical boundaries */
    Rect exterior;
    int  bnd_is_outer[4];
} DomainComponent;

typedef struct DomainGeometry {
    size_t domain_size[2];

    unsigned int  nb_components;
    DomainComponent *components;
} DomainGeometry;

DomainGeometry *mg2di_dg_alloc(unsigned int nb_components);
void            mg2di_dg_free(DomainGeometry **dg);

int             mg2di_dg_copy(DomainGeometry **dst, const DomainGeometry *src);

/**
 * Compute the intersection of src1 and src2, store the result in dst.
 */
int mg2di_rect_intersect(Rect *dst, const Rect *src1, const Rect *src2);

/**
 * For the component <comp> in domain geometry <dg>, internal edges are
 * rectangles along each non-outer boundary. Edge overlap with component <comp1>
 * is the intersection of internal edges of <comp> and exterior of <comp1> (must
 * necessarily be a rectangle).
 *
 * NB: edge overlap of a component with itself is empty.
 *
 * This function computes, for the given component <comp>
 * - edge overlaps of <comp> with every component in dg (i.e. the ghost points
 *   to be received from each remote component) - written into overlaps_recv
 * - edge overlaps of every component in dg with <comp> (i.e. the ghost points
 *   to be set to each remote component) - written into overlaps_send
 */
int mg2di_dg_edge_overlaps(Rect *overlaps_recv, Rect *overlaps_send,
                           const DomainGeometry *dg, unsigned int comp,
                           unsigned int edge_width);

#endif // MG2D_COMPONENTS_H
