/*
 * Copyright 2000-2017 the Libav developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_CPU_H
#define MG2D_CPU_H

#define MG2DI_CPU_FLAG_MMX          0x0001 ///< standard MMX
#define MG2DI_CPU_FLAG_MMXEXT       0x0002 ///< SSE integer functions or AMD MMX ext
#define MG2DI_CPU_FLAG_3DNOW        0x0004 ///< AMD 3DNOW
#define MG2DI_CPU_FLAG_SSE          0x0008 ///< SSE functions
#define MG2DI_CPU_FLAG_SSE2         0x0010 ///< PIV SSE2 functions
#define MG2DI_CPU_FLAG_SSE2SLOW 0x40000000 ///< SSE2 supported, but usually not faster
                                        ///< than regular MMX/SSE (e.g. Core1)
#define MG2DI_CPU_FLAG_3DNOWEXT     0x0020 ///< AMD 3DNowExt
#define MG2DI_CPU_FLAG_SSE3         0x0040 ///< Prescott SSE3 functions
#define MG2DI_CPU_FLAG_SSE3SLOW 0x20000000 ///< SSE3 supported, but usually not faster
                                        ///< than regular MMX/SSE (e.g. Core1)
#define MG2DI_CPU_FLAG_SSSE3        0x0080 ///< Conroe SSSE3 functions
#define MG2DI_CPU_FLAG_SSSE3SLOW 0x4000000 ///< SSSE3 supported, but usually not faster
#define MG2DI_CPU_FLAG_ATOM     0x10000000 ///< Atom processor, some SSSE3 instructions are slower
#define MG2DI_CPU_FLAG_SSE4         0x0100 ///< Penryn SSE4.1 functions
#define MG2DI_CPU_FLAG_SSE42        0x0200 ///< Nehalem SSE4.2 functions
#define MG2DI_CPU_FLAG_AVX          0x4000 ///< AVX functions: requires OS support even if YMM registers aren't used
#define MG2DI_CPU_FLAG_AVXSLOW   0x8000000 ///< AVX supported, but slow when using YMM registers (e.g. Bulldozer)
#define MG2DI_CPU_FLAG_XOP          0x0400 ///< Bulldozer XOP functions
#define MG2DI_CPU_FLAG_FMA4         0x0800 ///< Bulldozer FMA4 functions
#define MG2DI_CPU_FLAG_CMOV         0x1000 ///< i686 cmov
#define MG2DI_CPU_FLAG_AVX2         0x8000 ///< AVX2 functions: requires OS support even if YMM registers aren't used
#define MG2DI_CPU_FLAG_FMA3        0x10000 ///< Haswell FMA3 functions
#define MG2DI_CPU_FLAG_BMI1        0x20000 ///< Bit Manipulation Instruction Set 1
#define MG2DI_CPU_FLAG_BMI2        0x40000 ///< Bit Manipulation Instruction Set 2

int mg2di_cpu_flags_get(void);

void mg2di_cpu_cpuid(int index, int *eax, int *ebx, int *ecx, int *edx);
void mg2di_cpu_xgetbv(int op, int *eax, int *edx);

#endif /* MG2D_CPU_H */
