/*
 * Boundary condition declaration.
 * Copyright 2018 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_BOUNDARY_H
#define MG2D_BOUNDARY_H

#include <stddef.h>

typedef struct MG2DBoundaryInternal MG2DBoundaryInternal;

/**
 * Type of the boundary condition on a given boundary.
 */
enum MG2DBCType {
    /**
     * The value of the unknown function is fixed on the boundary.
     */
    MG2D_BC_TYPE_FIXVAL,
    /**
     * The unknown function is mirrored across the boundary
     */
    MG2D_BC_TYPE_REFLECT,

    /**
     * The unknown function falls off as C / x on the boundary.
     */
    MG2D_BC_TYPE_FALLOFF,
};

/**
 * Location of the boundary.
 */
enum MG2DBoundaryLoc {
    /**
     * Undefined boundary location.
     */
    MG2D_BOUNDARY_NONE = -1,
    /**
     * coord0 lower
     */
    MG2D_BOUNDARY_0L,
    /**
     * coord0 upper
     */
    MG2D_BOUNDARY_0U,
    /**
     * coord1 lower
     */
    MG2D_BOUNDARY_1L,
    /**
     * coord1 upper
     */
    MG2D_BOUNDARY_1U,
};

/**
 * Boundary condition definition.
 */
typedef struct MG2DBoundary {
    /**
     * Type of the boundary condition.
     */
    enum MG2DBCType type;
    /**
     * For type = MG2D_BC_TYPE_FIXVAL:
     *     Values of the unknown function on the boundary.
     *     The number of boundary layers is equal to fd_stencil.
     *     The first boundary layer has the number of points equal to the
     *     corresponding domain_size. Each subsequent boundary layer has one
     *     more boundary point at each end of the domain.
     *
     * Ignored otherwise.
     */
    double *val;
    /**
     * Number of elements between rows in val. I.e. if val[0] is the first
     * boundary point, then val[val_stride - 1] is the first boundary point in
     * the second row and so on.
     */
    size_t  val_stride;

    /**
     * Private data, not to be touched by callers.
     */
    MG2DBoundaryInternal *priv;
} MG2DBoundary;

/**
 * @return index of the coordinate that is kept constant along this boundary
 * (x: 0, y: 1)
 */
int mg2d_bnd_coord_idx(enum MG2DBoundaryLoc loc);

/**
 * @return 1 if this is the upper boundary, 0 for the lower one
 */
int mg2d_bnd_is_upper(enum MG2DBoundaryLoc loc);

/**
 * @return 1 if the corresponding variable increases in the direction from the
 * computational domain across the boundary, -1 if it decreases.
 *
 * @note this is equivalent to (mg2d_bnd_is_upper(loc) ? 1 : -1)
 */
int mg2d_bnd_out_dir(enum MG2DBoundaryLoc loc);

/**
 * Construct the boundary id from coordinate and upper/lower specification.
 */
enum MG2DBoundaryLoc mg2d_bnd_id(int coord_idx, int is_upper);

#endif // MG2D_BOUNDARY_H
