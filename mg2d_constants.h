/*
 * Public API constants.
 * Copyright 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_CONSTANTS_H
#define MG2D_CONSTANTS_H

enum MG2DError {
    /**
     * The solver has diverged.
     */
    MG2D_ERR_DIVERGE         = -0xff00,
    /* Maximum number of iterations has been executed without the solution
     * reaching desired tolerance.
     */
    MG2D_ERR_MAXITER_REACHED = -0xff01,
};

/**
 * Derivative specifier.
 */
enum MG2DDiffCoeff {
    /**
     * Zeroth derivative, i.e. function value.
     */
    MG2D_DIFF_COEFF_00,
    /**
     * First derivative wrt coord1.
     */
    MG2D_DIFF_COEFF_01,
    /**
     * First derivative wrt coord0.
     */
    MG2D_DIFF_COEFF_10,
    /**
     * Second derivative, once wrt coord0 and once wrt coord1
     */
    MG2D_DIFF_COEFF_11,
    /**
     * Second derivative wrt coord1
     */
    MG2D_DIFF_COEFF_02,
    /**
     * Second derivative wrt coord0
     */
    MG2D_DIFF_COEFF_20,
    /**
     * Total number of allowed derivatives.
     */
    MG2D_DIFF_COEFF_NB,
};

enum MG2DLogLevel {
    /**
     * The log message indicates abnormal program failure. Should never happen
     * unless there are bugs or grossly invaid user input.
     * Will typically be followed by program termination.
     */
    MG2D_LOG_FATAL = 0x0,
    /**
     * The log message indicates an error state, so that requested operation
     * cannot complete successfully.
     * Will typically be followed by the API function being called returning an
     * error.
     */
    MG2D_LOG_ERROR = 0x10,
    /**
     * The log message indicates a suspicious or suboptimal program state.
     */
    MG2D_LOG_WARNING = 0x20,
    /**
     * The log message provides commonly useful information about normal program
     * behaviour.
     */
    MG2D_LOG_INFO = 0x30,
    /**
     * The log message provides detailed extra information about normal program
     * behaviour.
     */
    MG2D_LOG_VERBOSE = 0x40,
    /**
     * The log message provides highly detailed extra information about normal
     * program behaviour.
     */
    MG2D_LOG_DEBUG = 0x50,
};

#endif // MG2D_CONSTANTS_H
