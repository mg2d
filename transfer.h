/*
 * Grid transfer operators
 * Copyright 2018 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_TRANSFER_H
#define MG2D_TRANSFER_H

#include <ndarray.h>
#include <stddef.h>
#include <threadpool.h>


enum GridTransferOperator {
    GRID_TRANSFER_LAGRANGE_1,
    GRID_TRANSFER_LAGRANGE_3,
    GRID_TRANSFER_LAGRANGE_5,
    GRID_TRANSFER_LAGRANGE_7,
    GRID_TRANSFER_FW_1,
    GRID_TRANSFER_FW_2,
    GRID_TRANSFER_FW_3,
    GRID_TRANSFER_FW_4,
};

/**
 * An N-dimensional grid with regular spacing.
 */
typedef struct RegularGrid {
    /**
     * Indices of the grid origin, relative to coordinate origin.
     * For each dimension i=0..N-1:
     *   o_i = start[i] * step[i]
     */
    ptrdiff_t          *start;

    /**
     * Number of points in the domain.
     */
    size_t             *size;

    /**
     * Size of the step between neighbouring grid points.
     */
    double             *step;
} RegularGrid;

typedef struct GridTransferContext {
    /**
     * Private data, not to be accessed by the caller.
     */
    void *priv;

    /**
     * Thread pool for parallel execution, must be set by the caller.
     */
    TPContext *tp;
    /**
     * CPU feature flags, must be set by the caller.
     */
    int cpuflags;

    /**
     * The operator used, set by mg2di_gt_alloc().
     */
    enum GridTransferOperator op;

    /**
     * Number of dimensions.
     */
    unsigned int nb_dims;

    /**
     * Source grid geometry, must be filled by the caller.
     */
    RegularGrid src;

    /**
     * Destination grid geometry, must be filled by the caller.
     */
    RegularGrid dst;

    /**
     * Allow extrapolating this many points beyond the source rectangle.
     * Defaults to 0: forbid extrapolation.
     */
    int extrapolate_distance;
} GridTransferContext;

/**
 * Allocate the transfer context for the given transfer operator.
 *
 * @return newly allocated transfer context on success, NULL on failure
 */
GridTransferContext *mg2di_gt_alloc(unsigned int dims, enum GridTransferOperator op);

/**
 * Initialize the tranfer context after all the parameters have been set.
 *
 * @return 0 on success, a negative error code on failure
 */
int mg2di_gt_init(GridTransferContext *ctx);

/**
 * Free the transfer context and everything associated with it.
 * Write NULL in the supplied pointer.
 */
void mg2di_gt_free(GridTransferContext **ctx);

/**
 * Execute the transfer.
 *
 * @param ctx transfer context
 * @param dst destination data, must match the dst grid in the transfer context
 * @param src source data, must match the src grid in the transfer context
 *
 * @return 0 on success, a negative error code on failure.
 */
int mg2di_gt_transfer(GridTransferContext *ctx,
                      NDArray *dst, const NDArray *src);

#endif // MG2D_TRANSFER_H
