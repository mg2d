/*
 * Boundary conditions internal API.
 * Copyright 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_BOUNDARY_INTERNAL_H
#define MG2D_BOUNDARY_INTERNAL_H

#include <stddef.h>

#include "mg2d_boundary.h"

/**
 * Allocate and return a new boundary context.
 *
 * @param domain_size number of points in the computational domain along the
 *                    boundary
 */
MG2DBoundary *mg2di_bc_alloc(size_t domain_size);

void mg2di_bc_free(MG2DBoundary **bnd);

#endif // MG2D_BOUNDARY_INTERNAL_H
