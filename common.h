/*
 * Copyright 2017 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_COMMON_H
#define MG2D_COMMON_H

#define SQR(x) ((x) * (x))
#define SGN(x) ((x) >= 0.0 ? 1.0 : -1.0)
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define ARRAY_ELEMS(arr) (sizeof(arr) / sizeof(*arr))

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define FD_STENCIL_MAX 4

#define STRINGIFY(x) #x
#define SOURCE_LOC(f, l) f ":" STRINGIFY(l)

#define mg2di_assert(x)                                     \
do {                                                        \
    if (!(x)) {                                             \
        fputs(SOURCE_LOC(__FILE__, __LINE__)                \
              ": " "Assertion " #x " failed\n", stderr);    \
        abort();                                            \
    }                                                       \
} while (0)

#endif /* MG2D_COMMON_H */
