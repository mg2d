/*
 * Copyright 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MG2D_TIMER_H
#define MG2D_TIMER_H

#include <stdint.h>
#include <time.h>

#include "common.h"

typedef struct Timer {
    int64_t time_nsec;
    int64_t nb_runs;
    int64_t start;
} Timer;

void mg2di_timer_init(Timer *t);

static inline void mg2di_timer_start(Timer *t)
{
    struct timespec tp;
    int ret;

    mg2di_assert(t->start == INT64_MIN);

    ret = clock_gettime(CLOCK_MONOTONIC, &tp);
    mg2di_assert(ret == 0);

    t->start = (int64_t)tp.tv_sec * 1000000000L + tp.tv_nsec;
}

static inline void mg2di_timer_stop(Timer *t)
{
    struct timespec tp;
    int ret;

    mg2di_assert(t->start != INT64_MIN);

    ret = clock_gettime(CLOCK_MONOTONIC, &tp);
    mg2di_assert(ret == 0);

    t->time_nsec += (int64_t)tp.tv_sec * 1000000000L + tp.tv_nsec - t->start;
    t->nb_runs++;
    t->start = INT64_MIN;
}

#endif // MG2D_TIMER_H
