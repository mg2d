/*
 * Boundary conditions internal API.
 * Copyright 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stddef.h>
#include <stdlib.h>

#include "common.h"
#include "mg2d_boundary.h"
#include "mg2d_boundary_internal.h"

struct MG2DBoundaryInternal {
    double *val_base;
};

MG2DBoundary *mg2di_bc_alloc(size_t domain_size)
{
    MG2DBoundary *bnd;
    size_t boundary_len_max;

    if (domain_size > SIZE_MAX - 2 * (FD_STENCIL_MAX - 1))
        return NULL;
    boundary_len_max = domain_size + 2 * (FD_STENCIL_MAX - 1);
    if (boundary_len_max > SIZE_MAX / (FD_STENCIL_MAX * sizeof(bnd->val)))
        return NULL;

    bnd = calloc(1, sizeof(*bnd));
    if (!bnd)
        return NULL;

    bnd->priv = calloc(1, sizeof(*bnd->priv));
    if (!bnd->priv) {
        free(bnd);
        return NULL;
    }

    bnd->priv->val_base = malloc(boundary_len_max * FD_STENCIL_MAX * sizeof(*bnd->val));
    if (!bnd->priv->val_base)
        goto fail;

    bnd->val        = bnd->priv->val_base + FD_STENCIL_MAX - 1;
    bnd->val_stride = boundary_len_max;

    return bnd;
fail:
    mg2di_bc_free(&bnd);
    return NULL;
}

void mg2di_bc_free(MG2DBoundary **pbnd)
{
    MG2DBoundary *bnd = *pbnd;

    if (!bnd)
        return;

    free(bnd->priv->val_base);
    free(bnd->priv);
    free(bnd);
    *pbnd = NULL;
}

static const struct {
    unsigned int coord_idx;
    unsigned int is_upper;
} boundary_def[] = {
    [MG2D_BOUNDARY_0L] = {
        .coord_idx = 0,
        .is_upper  = 0,
    },
    [MG2D_BOUNDARY_0U] = {
        .coord_idx = 0,
        .is_upper  = 1,
    },
    [MG2D_BOUNDARY_1L] = {
        .coord_idx = 1,
        .is_upper  = 0
    },
    [MG2D_BOUNDARY_1U] = {
        .coord_idx = 1,
        .is_upper  = 1,
    },
};

int mg2d_bnd_coord_idx(enum MG2DBoundaryLoc loc)
{
    if (loc < 0 || loc >= ARRAY_ELEMS(boundary_def))
        return -EINVAL;

    return boundary_def[loc].coord_idx;
}

int mg2d_bnd_is_upper(enum MG2DBoundaryLoc loc)
{
    if (loc < 0 || loc >= ARRAY_ELEMS(boundary_def))
        return -EINVAL;

    return boundary_def[loc].is_upper;
}

int mg2d_bnd_out_dir(enum MG2DBoundaryLoc loc)
{
    int ret = mg2d_bnd_is_upper(loc);

    if (ret < 0)
        return 0;

    return ret ? 1 : -1;
}

enum MG2DBoundaryLoc mg2d_bnd_id(int coord_idx, int is_upper)
{
    for (int i = 0; i < ARRAY_ELEMS(boundary_def); i++) {
        if (boundary_def[i].coord_idx == coord_idx &&
            boundary_def[i].is_upper  == is_upper)
            return i;
    }
    return MG2D_BOUNDARY_NONE;
}
