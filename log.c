/*
 * Copyright 2014-2017 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * logging code
 */

#include <stdarg.h>
#include <stdio.h>

#include "log.h"

void mg2di_log_default_callback(MG2DLogger *log, int level,
                                const char *fmt, va_list vl)
{
    vfprintf(stderr, fmt, vl);
}

void mg2di_vlog(MG2DLogger *log, int level, const char *fmt, va_list vl)
{
    log->log(log, level, fmt, vl);
}

void mg2di_log(MG2DLogger *log, int level, const char *fmt, ...)
{
    va_list vl;

    if (!log->log)
        return;

    va_start(vl, fmt);
    mg2di_vlog(log, level, fmt, vl);
    va_end(vl);
}
