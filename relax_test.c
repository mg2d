#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ndarray.h>

#include "ell_grid_solve.h"
#include "log.h"
#include "mg2d_boundary.h"
#include "mg2d_constants.h"

#define ARRAY_ELEMS(x) (sizeof(x) / sizeof(*x))
#define DOMAIN_SIZE 1.0

#if 1
static double sol(double x, double y)
{
    return sin(M_PI * x) * sin(M_PI * y);
}
static double sol_dx(double x, double y)
{
    return M_PI * cos(M_PI * x) * sin(M_PI * y);
}
static double sol_dy(double x, double y)
{
    return M_PI * sin(M_PI * x) * cos(M_PI * y);
}
static double sol_dxx(double x, double y)
{
    return -M_PI * M_PI * sol(x, y);
}
static double sol_dyy(double x, double y)
{
    return -M_PI * M_PI * sol(x, y);
}
static double sol_dxy(double x, double y)
{
    return M_PI * M_PI * cos(M_PI * x) * cos(M_PI * y);
}
#endif

int main(int argc, char **argv)
{
    EGSContext *ctx;
    long int log2N, log2maxiter;
    long int N, maxiter;
    double res_old, res_new;
    int ret = 0;

    if (argc < 3) {
        fprintf(stderr, "Usage: %s <log2 N> <log2 maxiter>\n", argv[0]);
        return 1;
    }
    log2N       = strtol(argv[1], NULL, 0);
    log2maxiter = strtol(argv[2], NULL, 0);
    if (log2N <= 0 || log2maxiter < 0 ||
        log2N >= sizeof(N) * 8 || log2maxiter >= sizeof(maxiter) * 8) {
        fprintf(stderr, "Invalid log2N/log2maxiter values: %ld/%ld\n",
                log2N, log2maxiter);
        return 1;
    }

    N       = (1L << log2N) + 1;
    maxiter = 1L << log2maxiter;

    ctx = mg2di_egs_alloc((size_t [2]){N, N});
    if (!ctx) {
        fprintf(stderr, "Error allocating the solver context\n");
        return 1;
    }

    ctx->logger.log = mg2di_log_default_callback;

    ctx->step[0] = DOMAIN_SIZE / (N - 1.0);
    ctx->step[1] = DOMAIN_SIZE / (N - 1.0);

    ctx->fd_stencil = 2;

    for (int bnd_loc = 0; bnd_loc < ARRAY_ELEMS(ctx->boundaries); bnd_loc++) {
        MG2DBoundary *bnd = ctx->boundaries[bnd_loc];
        const int ci      = mg2d_bnd_coord_idx(bnd_loc);
        const int bnd_dir = mg2d_bnd_out_dir(bnd_loc);

        double coord[2];

        bnd->type = MG2D_BC_TYPE_FIXVAL;

        memset(bnd->val, 0, N * sizeof(*bnd->val));

        for (int j = 1; j < ctx->fd_stencil; j++) {
            double *dst = bnd->val + j * bnd->val_stride;

            coord[ci] = mg2d_bnd_is_upper(bnd_loc) * DOMAIN_SIZE + bnd_dir * j * ctx->step[ci];

            for (ptrdiff_t k = -j; k < N + j; k++) {
                coord[!ci] = k * ctx->step[!ci];
                dst[k] = sol(coord[0], coord[1]);
            }
        }
    }

    for (size_t y = 0; y < ctx->domain_size[1]; y++) {
        const double y_coord = y * ctx->step[1];

        memset(NDA_PTR2D(ctx->u, 0, y), 0, sizeof(*ctx->u->data) * ctx->domain_size[0]);

        for (size_t x = 0; x < ctx->domain_size[0]; x++) {
            const double x_coord = x * ctx->step[0];

            *NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_02], x, y) =  1.0;
            *NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_20], x, y) =  1.0;
            *NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_11], x, y) =  1.0;

            *NDA_PTR2D(ctx->rhs, x, y) = sol_dxx(x_coord, y_coord) + sol_dyy(x_coord, y_coord) + sol_dxy(x_coord, y_coord);
        }

        memset(NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_00], 0, y), 0,
               sizeof(*ctx->diff_coeffs[0]->data) * ctx->domain_size[0]);
        memset(NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_01], 0, y), 0,
               sizeof(*ctx->diff_coeffs[0]->data) * ctx->domain_size[0]);
        memset(NDA_PTR2D(ctx->diff_coeffs[MG2D_DIFF_COEFF_10], 0, y), 0,
               sizeof(*ctx->diff_coeffs[0]->data) * ctx->domain_size[0]);
    }

    ret = mg2di_egs_init(ctx, 0);
    if (ret < 0) {
        fprintf(stderr, "Error initializing the solver context: %d\n", ret);
        ret = 1;
        goto fail;
    }

    res_old = ctx->residual_max;

    for (int i = 0; i < maxiter; i++) {
        ret = mg2di_egs_solve(ctx, EGS_SOLVE_RELAXATION, 0);
        if (ret < 0) {
            fprintf(stderr, "Error during relaxation\n");
            ret = 1;
            goto fail;
        }
        res_new = ctx->residual_max;
        if (res_new > 1e3) {
            fprintf(stderr, "Diverged at step %d: %g -> %g\n", i, res_old, res_new);
            goto fail;
        }
        fprintf(stderr, "Step %d: %g -> %g (%g)\n", i, res_old, res_new, res_old / res_new);
        res_old = res_new;
    }

    {
        double max_err = 0.0;

        for (size_t y = 0; y < ctx->domain_size[1]; y++) {
            const double y_coord = y * ctx->step[1];

            for (size_t x = 0; x < ctx->domain_size[0]; x++) {
                const double x_coord = x * ctx->step[0];
                double err = fabs(ctx->u->data[y * ctx->u->stride[0] + x] - sol(x_coord, y_coord));
                if (err > max_err)
                    max_err = err;
            }
        }
        fprintf(stderr, "max(|solution - exact|): %g\n", max_err);
    }

fail:
    mg2di_egs_free(&ctx);
    return ret;
}
